-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: May 18, 2016 at 02:11 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movote`
--
CREATE DATABASE IF NOT EXISTS `movote` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `movote`;

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE `films` (
  `film_id` varchar(100) NOT NULL,
  `film_title` varchar(100) NOT NULL,
  `film_about` text NOT NULL,
  `film_scrore` int(11) NOT NULL,
  `film_image_url` varchar(100) NOT NULL,
  `film_image_filename` varchar(100) NOT NULL,
  `film_image_location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`film_id`, `film_title`, `film_about`, `film_scrore`, `film_image_url`, `film_image_filename`, `film_image_location`) VALUES
('e1fbbe0a-1bf6-11e6-9b33-68f9a759c9e5', 'Captain America: Civil War', 'Political interference in the Avengers'' activities causes a rift between former allies Captain America and Iron Man.', 0, '', '', ''),
('e1fbd0de-1bf6-11e6-9b33-68f9a759c9e5', 'Suicide Squad', 'A secret government agency recruits imprisoned supervillains to execute dangerous black ops missions in exchange for clemency.', 0, '', '', ''),
('e1fbdd04-1bf6-11e6-9b33-68f9a759c9e5', 'Doctor Strange', 'After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil.', 0, '', '', ''),
('e1fbea2e-1bf6-11e6-9b33-68f9a759c9e5', 'Finding Dory', 'The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way.', 0, '', '', ''),
('e1fc0284-1bf6-11e6-9b33-68f9a759c9e5', 'Independence Day: Resurgence', 'Two decades after the first Independence Day invasion, Earth is faced with a new extra-Solar threat. But will mankind''s new space defenses be enough?', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `film_category`
--

CREATE TABLE `film_category` (
  `category` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film_category`
--

INSERT INTO `film_category` (`category`) VALUES
('Comedic Genius'),
('Favourite'),
('Mind Bending'),
('Most Anticipated'),
('Shocking Plot Twist'),
('Underrated Movie');

-- --------------------------------------------------------

--
-- Table structure for table `film_genre`
--

CREATE TABLE `film_genre` (
  `film_id` varchar(100) NOT NULL,
  `genre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film_genre`
--

INSERT INTO `film_genre` (`film_id`, `genre`) VALUES
('e1fbbe0a-1bf6-11e6-9b33-68f9a759c9e5', 'Action'),
('e1fbd0de-1bf6-11e6-9b33-68f9a759c9e5', 'Action'),
('e1fbbe0a-1bf6-11e6-9b33-68f9a759c9e5', 'Adventure '),
('e1fbd0de-1bf6-11e6-9b33-68f9a759c9e5', 'Adventure '),
('e1fbbe0a-1bf6-11e6-9b33-68f9a759c9e5', 'Sci-Fi');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `genre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`genre`) VALUES
('Action'),
('Adventure '),
('Animation'),
('Comedy'),
('Family'),
('Fantasy'),
('Sci-Fi'),
('Thriller');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_05_17_015724_create_social_accounts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `user_id` int(11) NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`user_id`, `provider_user_id`, `provider`, `created_at`, `updated_at`) VALUES
(1, '10204780647356539', 'facebook', '2016-05-16 16:19:52', '2016-05-16 16:19:52'),
(1, '10204780647356539', 'FacebookProvider', '2016-05-16 16:34:20', '2016-05-16 16:34:20'),
(1, '102386296880386730770', 'GoogleProvider', '2016-05-16 19:01:06', '2016-05-16 19:01:06'),
(2, '113176050422385889448', 'GoogleProvider', '2016-05-17 02:06:44', '2016-05-17 02:06:44'),
(3, '1219839484693584', 'FacebookProvider', '2016-05-17 17:59:17', '2016-05-17 17:59:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'William Henry', 'william.hidayat@gmail.com', '', 'o5epD7tcraATsakvby5gjLK9IhDBTPwitghAnNEHQ0iH1OIPgkWsA9Nlhowf', '2016-05-16 16:19:52', '2016-05-17 03:10:20'),
(2, 'william henry', 'william.hidayat16@gmail.com', '', 'boNa1qfPITOYfnH5b3vsUyqlp6ukdgbhZ6Fi8YzYZhMAAv9vHhM5uvuEjHXx', '2016-05-17 02:06:44', '2016-05-17 03:19:13'),
(3, 'Agung Wirayogi', 'agungwy@gmail.com', '', 'rqkG0rtA6TJO0L9p2dqhxYtGONqSwRvfUZVbK44IggXgIABkGQ7xklIpQ8zv', '2016-05-17 17:59:17', '2016-05-17 17:59:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`film_id`);

--
-- Indexes for table `film_category`
--
ALTER TABLE `film_category`
  ADD PRIMARY KEY (`category`);

--
-- Indexes for table `film_genre`
--
ALTER TABLE `film_genre`
  ADD PRIMARY KEY (`film_id`,`genre`),
  ADD KEY `fk_genre_film_genre` (`genre`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`genre`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `film_genre`
--
ALTER TABLE `film_genre`
  ADD CONSTRAINT `fk_films_film_genre` FOREIGN KEY (`film_id`) REFERENCES `films` (`film_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_genre_film_genre` FOREIGN KEY (`genre`) REFERENCES `genre` (`genre`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
