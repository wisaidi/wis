-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: May 28, 2016 at 03:45 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movote`
--

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`user_id`, `provider_user_id`, `provider`, `created_at`, `updated_at`) VALUES
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', '10206553096809480', 'FacebookProvider', '2016-05-27 21:41:19', '2016-05-27 21:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', 'Yanuar Wicaksana', 'yanu_96@yahoo.co.id', '', NULL, '2016-05-27 21:41:18', '2016-05-27 21:41:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_category_point`
--

CREATE TABLE `user_category_point` (
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `point` int(3) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_category_point`
--

INSERT INTO `user_category_point` (`user_id`, `category`, `point`) VALUES
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', 'Comedic Genius', 0),
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', 'Favourite', 0),
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', 'Mind Bending', 10),
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', 'Most Anticipated', 0),
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', 'Shocking Plot Twist', 10),
('yanu_96@yahoo.co.id57494b9ef1f538.05596383', 'Underrated Movie', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_category_point`
--
ALTER TABLE `user_category_point`
  ADD PRIMARY KEY (`user_id`,`category`),
  ADD KEY `fk_category` (`category`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_category_point`
--
ALTER TABLE `user_category_point`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category`) REFERENCES `film_category` (`category`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
