-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: May 28, 2016 at 09:54 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movote`
--
CREATE DATABASE IF NOT EXISTS `movote` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `movote`;

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE IF NOT EXISTS `films` (
  `film_id` varchar(100) NOT NULL,
  `film_title` varchar(100) NOT NULL,
  `film_about` text NOT NULL,
  `film_scrore` int(11) NOT NULL,
  `film_image_url` varchar(100) NOT NULL,
  `film_image_filename` varchar(100) NOT NULL,
  `film_image_filepath` varchar(100) NOT NULL,
  `category` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`film_id`, `film_title`, `film_about`, `film_scrore`, `film_image_url`, `film_image_filename`, `film_image_filepath`, `category`) VALUES
('e1fbbe0a-1bf6-11e6-9b33-68f9a759c9e5', 'Captain America: Civil War', 'Political interference in the Avengers'' activities causes a rift between former allies Captain America and Iron Man.', 0, '/image/e1fbbe0a-1bf6-11e6-9b33-68f9a759c9e5', 'cw3.jpg', '/image/cw3.jpg', 'Favourite'),
('e1fbd0de-1bf6-11e6-9b33-68f9a759c9e5', 'Suicide Squad', 'A secret government agency recruits imprisoned supervillains to execute dangerous black ops missions in exchange for clemency.', 0, '/image/e1fbd0de-1bf6-11e6-9b33-68f9a759c9e5', 'sc3.jpg', '/image/sc3.jpg', 'Most Anticipated'),
('e1fbdd04-1bf6-11e6-9b33-68f9a759c9e5', 'Doctor Strange', 'After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil.', 0, '/image/e1fbdd04-1bf6-11e6-9b33-68f9a759c9e5', 'ds.jpg', '/image/ds.jpg', 'Most Anticipated'),
('e1fbea2e-1bf6-11e6-9b33-68f9a759c9e5', 'Finding Dory', 'The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way.', 0, '/image/e1fbea2e-1bf6-11e6-9b33-68f9a759c9e5', 'fd.jpg', '/image/fd.jpg', 'Most Anticipated'),
('e1fc0284-1bf6-11e6-9b33-68f9a759c9e5', 'Independence Day: Resurgence', 'Two decades after the first Independence Day invasion, Earth is faced with a new extra-Solar threat. But will mankind''s new space defenses be enough?', 0, '/image/e1fc0284-1bf6-11e6-9b33-68f9a759c9e5', 'id.jpg', '/image/id.jpg', 'Most Anticipated'),
('fa0f24a4-21bf-11e6-b07a-c8cdaaeda574', 'X-Men: Apocalypse\r\n', 'With the emergence of the world''s first mutant, Apocalypse, the X-Men must unite to defeat his extinction level plan.\r\n\r\n', 0, '/image/fa0f24a4-21bf-11e6-b07a-c8cdaaeda574', 'xmen.jpg', '/image/xmen.jpg', 'Favourite'),
('fa0f3b74-21bf-11e6-b07a-c8cdaaeda574', 'Batman v Superman\r\n', 'Fearing that the actions of Superman are left unchecked, Batman takes on the Man of Steel, while the world wrestles with what kind of a hero it really needs.\r\n', 0, '/image/fa0f3b74-21bf-11e6-b07a-c8cdaaeda574', 'bvs3.jpg', '/image/bvs3.jpg', 'Favourite'),
('fa0f4a60-21bf-11e6-b07a-c8cdaaeda574', 'Zootopia\r\n', 'In a city of anthropomorphic animals, a rookie bunny cop and a cynical con artist fox must work together to uncover a conspiracy.\r\n\r\n', 0, '/image/fa0f4a60-21bf-11e6-b07a-c8cdaaeda574', 'zoo.jpg', '/image/zoo.jpg', 'Favourite'),
('fa0f55e6-21bf-11e6-b07a-c8cdaaeda574', 'The Jungle Book\r\n', 'After a threat from the tiger Shere Khan forces him to flee the jungle, a man-cub named Mowgli embarks on a journey of self discovery with the help of panther, Bagheera, and free spirited bear, Baloo.\r\n\r\n', 0, '/image/fa0f55e6-21bf-11e6-b07a-c8cdaaeda574', 'jb3.jpg', '/image/jb3.jpg', 'Favourite'),
('fa0f62d4-21bf-11e6-b07a-c8cdaaeda574', 'Fantastic Beast and Where to Find Them\r\n', 'The adventures of writer Newt Scamander in New York''s secret community of witches and wizards seventy years before Harry Potter reads his book in school.\r\n\r\n', 0, '/image/fa0f62d4-21bf-11e6-b07a-c8cdaaeda574', 'fb.jpg', '/image/fb.jpg', 'Most Anticipated'),
('fa0f7094-21bf-11e6-b07a-c8cdaaeda574', 'Mad Max: Fury Road\r\n', 'A woman rebels against a tyrannical ruler in postapocalyptic Australia in search for her home-land with the help of a group of female prisoners, a psychotic worshipper, and a drifter named Max.\r\n\r\n', 0, '/image/fa0f7094-21bf-11e6-b07a-c8cdaaeda574', 'mm.jpg', '/image/mm.jpg', 'Shocking Plot Twist'),
('fa0f7dbe-21bf-11e6-b07a-c8cdaaeda574', 'Inside Out\r\n', 'After young Riley is uprooted from her Midwest life and moved to San Francisco, her emotions - Joy, Fear, Anger, Disgust and Sadness - conflict on how best to navigate a new city, house, and school.\r\n', 0, '/image/fa0f7dbe-21bf-11e6-b07a-c8cdaaeda574', 'io.jpg', '/image/io.jpg', 'Shocking Plot Twist'),
('fa0f89a8-21bf-11e6-b07a-c8cdaaeda574', 'Ex Machina\r\n', 'A young programmer is selected to participate in a ground-breaking experiment in synthetic intelligence by evaluating the human qualities of a breath-taking humanoid A.I.\r\n', 0, '/image/fa0f89a8-21bf-11e6-b07a-c8cdaaeda574', 'em.jpg', '/image/em.jpg', 'Shocking Plot Twist'),
('fa0f95c4-21bf-11e6-b07a-c8cdaaeda574', 'Sicario\r\n', 'An idealistic FBI agent is enlisted by a government task force to aid in the escalating war against drugs at the border area between the U.S. and Mexico.\r\n', 0, '/image/fa0f95c4-21bf-11e6-b07a-c8cdaaeda574', 'so.jpg', '/image/so.jpg', 'Shocking Plot Twist'),
('fa0faa32-21bf-11e6-b07a-c8cdaaeda574', 'The Gift\r\n', 'A young married couple''s lives are thrown into a harrowing tailspin when an acquaintance from the husband''s past brings mysterious gifts and a horrifying secret to light after more than 20 years.\r\n', 0, '/image/fa0faa32-21bf-11e6-b07a-c8cdaaeda574', 'tg.jpg', '/image/tg.jpg', 'Shocking Plot Twist'),
('fa0fd034-21bf-11e6-b07a-c8cdaaeda574', 'Legend\r\n', 'The film tells the story of the identical twin gangsters Reggie and Ronnie Kray, two of the most notorious criminals in British history, and their organised crime empire in the East End of London during the 1960s.\r\n\r\n', 0, '/image/fa0fd034-21bf-11e6-b07a-c8cdaaeda574', 'lg.jpg', '/image/lg.jpg', 'Underrated Movie'),
('fa0fe222-21bf-11e6-b07a-c8cdaaeda574', 'Focus', 'In the midst of veteran con man Nicky''s latest scheme, a woman from his past - now an accomplished femme fatale - shows up and throws his plans for a loop.\r\n', 0, '/image/fa0fe222-21bf-11e6-b07a-c8cdaaeda574', 'focus.jpg', '/image/focus.jpg', 'Underrated Movie'),
('fa0ff41a-21bf-11e6-b07a-c8cdaaeda574', 'The Salvation', 'In 1870s America, a peaceful American settler kills his family''s murderer which unleashes the fury of a notorious gang leader. His cowardly fellow townspeople then betray him, forcing him to hunt down the outlaws alone.\r\n', 0, '/image/fa0ff41a-21bf-11e6-b07a-c8cdaaeda574', 'sv.jpg', '/image/sv.jpg', 'Underrated Movie'),
('fa1001ee-21bf-11e6-b07a-c8cdaaeda574', 'Kill Your Friends\r\n', 'An A&R man working at the height of the Britpop music craze goes to extremes in order to find his next hit.\r\n', 0, '/image/fa1001ee-21bf-11e6-b07a-c8cdaaeda574', 'kyf.jpg', '/image/kyf.jpg', 'Underrated Movie'),
('fa10130a-21bf-11e6-b07a-c8cdaaeda574', 'Man Up\r\n', 'A single woman takes the place of a stranger''s blind date, which leads to her finding the perfect boyfriend.\r\n\r\n', 0, '/image/fa10130a-21bf-11e6-b07a-c8cdaaeda574', 'mu.jpg', '/image/mu.jpg', 'Underrated Movie'),
('fa101f80-21bf-11e6-b07a-c8cdaaeda574', 'Deadpool\r\n', 'A former Special Forces operative turned mercenary is subjected to a rogue experiment that leaves him with accelerated healing powers, adopting the alter ego Deadpool.\r\n', 0, '/image/fa101f80-21bf-11e6-b07a-c8cdaaeda574', 'dp.jpg', '/image/dp.jpg', 'Comedic Genius'),
('fa102e26-21bf-11e6-b07a-c8cdaaeda574', 'Kung Fu Panda 3\r\n', 'Continuing his "legendary adventures of awesomeness", Po must face two hugely epic, but different threats: one supernatural and the other a little closer to his home.\r\n', 0, '/image/fa102e26-21bf-11e6-b07a-c8cdaaeda574', 'kp.jpg', '/image/kp.jpg', 'Comedic Genius'),
('fa104456-21bf-11e6-b07a-c8cdaaeda574', 'Hail, Caesar!\r\n', 'Follows a day in the life of Eddie Mannix, a Hollywood fixer for Capitol Pictures in the 1950s, who cleans up and solves problems for big names and stars in the industry. But when studio star Baird Whitlock disappears, Mannix has to deal with more than just the fix.\r\n', 0, '/image/fa104456-21bf-11e6-b07a-c8cdaaeda574', 'hc.jpg', '/image/hc.jpg', 'Comedic Genius'),
('fa105806-21bf-11e6-b07a-c8cdaaeda574', 'Keanu', 'Friends hatch a plot to retrieve a stolen kitten by posing as drug dealers for a street gang.\r\n', 0, '/image/fa105806-21bf-11e6-b07a-c8cdaaeda574', 'keanu.jpg', '/image/keanu.jpg', 'Comedic Genius'),
('fa106490-21bf-11e6-b07a-c8cdaaeda574', 'Predestination\r\n', 'The life of a time-traveling Temporal Agent. On his final assignment, he must pursue the one criminal that has eluded him throughout time.\r\n', 0, '/image/fa106490-21bf-11e6-b07a-c8cdaaeda574', 'pd.jpg', '/image/pd.jpg', 'Mind Bending'),
('fa10719c-21bf-11e6-b07a-c8cdaaeda574', 'Source Code\r\n', 'A soldier wakes up in someone else''s body and discovers he''s part of an experimental government program to find the bomber of a commuter train. A mission he has only 8 minutes to complete.\r\n', 0, '/image/fa10719c-21bf-11e6-b07a-c8cdaaeda574', 'sc.jpg', '/image/sc.jpg', 'Mind Bending'),
('fa107d36-21bf-11e6-b07a-c8cdaaeda574', 'Limitless\r\n', 'With the help of a mysterious pill that enables the user to access 100 percent of his brain abilities, a struggling writer becomes a financial wizard, but it also puts him in a new world with lots of dangers.\r\n\r\n', 0, '/image/fa107d36-21bf-11e6-b07a-c8cdaaeda574', 'lim.jpg', '/image/lim.jpg', 'Mind Bending'),
('fa108c2c-21bf-11e6-b07a-c8cdaaeda574', 'Inception', 'A thief, who steals corporate secrets through use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.\r\n\r\n', 0, '/image/fa108c2c-21bf-11e6-b07a-c8cdaaeda574', 'inc.jpg', '/image/inc.jpg', 'Mind Bending'),
('fa109910-21bf-11e6-b07a-c8cdaaeda574', 'The Butterfly Effect\r\n', 'Evan Treborn suffers blackouts during significant events of his life. As he grows up, he finds a way to remember these lost memories and a supernatural way to alter his life by reading his journal.\r\n', 0, '/image/fa109910-21bf-11e6-b07a-c8cdaaeda574', 'be.jpg', '/image/be.jpg', 'Mind Bending'),
('fa10a70c-21bf-11e6-b07a-c8cdaaeda574', 'The Nice Guys', 'A mismatched pair of private eyes investigate the apparent suicide of a fading porn star in 1970s Los Angeles.\r\n', 0, '/image/fa10a70c-21bf-11e6-b07a-c8cdaaeda574', 'ng.jpg', '/image/ng.jpg', 'Comedic Genius');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`film_id`,`category`),
  ADD KEY `fk_films_film_category` (`category`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `films`
--
ALTER TABLE `films`
  ADD CONSTRAINT `fk_films_film_category` FOREIGN KEY (`category`) REFERENCES `film_category` (`category`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
