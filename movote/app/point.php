<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class point extends Model
{
    protected $table="user_category_point";
    public $timestamps=false;
    public $incrementing=false;
    protected $fillable = ["user_id","category","point"];
}
