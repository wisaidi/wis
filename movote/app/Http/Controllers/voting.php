<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\point;
use App\FilmsModel;

class voting extends Controller
{
   
	public function __construct(){
	    $this->middleware('auth');
	}

    
    public function vote(Request $request){
        $data=$request->all();
/*         print_r($data); */
        $todo=point::where('user_id','=',$data['user_id'])
        			->where('category','=',$data['category']);
        			
        $point=$todo->get()->pluck('point')[0];
        //echo($point);
		
		if($point>0){
	        $todo->update(["point"=>($point-1)]);
			$film=FilmsModel::where('film_id','=',$data['film_id']);
			$movie=$film->get()->pluck('film_score')[0];
			$film->update(["film_score"=>($movie+1)]);
			
			return [
				"message"=>"Success",
				"film_score"=>$movie+1,
				"point"=>$point-1
			];
        } else {
	        return response()->json(["message"=>"Sorry, you have no points left"],404);
        }
			
    }
    
    public function score(Request $request){
	    $data=$request->all();
	    
	    $todo=point::where('user_id','=',$data['user_id'])
        			->where('category','=',$data['category']);
        			
        $point=$todo->get()->pluck('point')[0];
        
        return [
        	"point"=>$point
        
        ];
    }
    
    
}
