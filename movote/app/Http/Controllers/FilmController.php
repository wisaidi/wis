<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\FilmsModel;
use DB;

class FilmController extends Controller
{
    //
    
    public function view($category){
        $data=explode('/',$category)[0];
//        echo($data);
        $todos=FilmsModel::where('category','=',$data)->get();
        if(count($todos)>0){
            foreach($todos as $todo){
                $todo->genre;
            }
//            return $todos;
            return view('category')->with('data',$todos)
                                   ->with('category',$data);
        }else{
            return response()->json(["message"=>'Not Found'],404);
        }
        
        
    }

    public function getTop(){
        $films=DB::table('films')->orderBy('film_score','desc')->get();
        return $films;

    }
    
    public function poster($filmID){
        $data=explode('/',$filmID)[0];
        $todos=FilmsModel::where('film_id','=',$filmID)->get();
        if(count($todos)>0){
            $path=$todos->pluck('film_image_filepath')[0];
            $filename= $todos->pluck('film_image_filename')[0];
            return  response()->download(public_path().$path, $filename, ['Content-Type'=>'image/jpg']);
        }else{
            return response()->json(["message"=>'Not Found'],404);
        }
    }
    
    
}
