<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
Route::get('/callback/{provider}', 'SocialAuthController@callback');

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/vote', 'VoteController@index');
Route::get('/about',function(){
    return view('about');
});

Route::get('/category/{category}','FilmController@view');
Route::get('/image/{filmID}','FilmController@poster');

Route::get('/top','FilmController@getTop');
Route::post('/voting', 'voting@vote');
Route::get('/voting', 'voting@score');