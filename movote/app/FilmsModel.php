<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmsModel extends Model
{
    //
    protected $table="films";
    public $timestamps=false;
    public $incrementing=false;
    protected $primaryKey="film_id";
    public function genre(){
        return $this->hasMany('App\FilmGenreModel','film_id');
    }
}
