<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmGenreModel extends Model
{
    //
    protected $table="film_genre";
    public $incrementing=false;
    public $timestamps=false;
    protected $hidden=["film_id"];
}
