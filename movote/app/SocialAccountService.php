<?php

namespace App;
use App\point;

use Laravel\Socialite\Contracts\Provider;

class SocialAccountService
{
    public function createOrGetUser(Provider $provider)
    {

        $providerUser = $provider->user();
        $providerName = class_basename($provider); 

        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
				$unique = uniqid($providerUser->getEmail(),true);
				
				$user = User::create([
                	'id' => $unique,
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                ]);

		    	point::create([
		    		'user_id' => $unique,
		    		'category' => 'Comedic Genius',
		    		'point' => 10,
		    	]);
		    	point::create([
		    		'user_id' => $unique,
		    		'category' => 'Favourite',
		    		'point' => 10,
		    	]);
		    	point::create([
		    		'user_id' => $unique,
		    		'category' => 'Mind Bending',
		    		'point' => 10,
		    	]);
		    	point::create([
		    		'user_id' => $unique,
		    		'category' => 'Most Anticipated',
		    		'point' => 10,
		    	]);
		    	point::create([
		    		'user_id' => $unique,
		    		'category' => 'Shocking Plot Twist',
		    		'point' => 10,
		    	]);
		    	point::create([
		    		'user_id' => $unique,
		    		'category' => 'Underrated Movie',
		    		'point' => 10,
		    	]);

                            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}