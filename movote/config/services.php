<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '513317312203613',
        'client_secret' => '6c0e117867b35fb77004d4b2acd4a063',
        'redirect' => 'http://movote.azurewebsites.net/callback/facebook',
    ],
    'twitter' => [
        'client_id' => 'dmvbhTSVmYXH7JkUm3TRFpeRt',
        'client_secret' => 'dWnWu1FH5BNcdb8V8OMP74FcFA2q6OCeU2SyaCCjKVy9ipbJ8E',
        'redirect' => 'http://localhost:8080/callback/twitter',
    ],
    'google' => [
        'client_id' => '672036156361-2lc3s999f3k9g4cqif0i618evcre5tt5.apps.googleusercontent.com',
        'client_secret' => 'ly_8ZX6fvs51uhOJRw2ixriX',
        'redirect' => 'https://movote.azurewebsites.net/callback/google',
    ],

];
