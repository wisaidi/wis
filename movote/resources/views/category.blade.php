@extends('layouts.app')

@section('head')
@endsection

@section('content')

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo($category)?></h1>
            </div>
            @if (!Auth::guest())  
            <h3 id = "userpoints" class="text-right"></h3>
            @endif
        </div>
        <!-- /.row -->
        
        <div class="content-category"></div>

<!--
        <div class="row">
            <div class="col-md-7">
                <a>
                    <img class="img-responsive img-hover" src="{{url('image/e1fbbe0a-1bf6-11e6-9b33-68f9a759c9e5')}}" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Captain America: Civil War</h3>
                <h5>Action | Adventure | Sci-Fi</h5>
                <p>Political interference in the Avengers' activities causes a rift between former allies Captain America and Iron Man.</p>
                <br>
                <h4 class="text-center">Score: </h4> 
                <h3 id = "score1" class="text-center">4024</h3>
                <br>
                @if (!Auth::guest())
                <a class="btn btn-primary btn-lg center-block" onclick="incrementScore1()">Vote</a>
                @else
                <a class="btn btn-primary btn-lg center-block" href="{{url('login')}}">Login</a>
                @endif
            </div>
        </div>
-->
        <!-- /.row -->

<!--        <hr>-->

<!--
        <div class="row">
            <div class="col-md-7">
                <a>
                    <img class="img-responsive img-hover" src="image/sc3.jpg" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Suicide Squad</h3>
                <h5>Action | Adventure | Fantasy | Sci-Fi | Thriller</h5>
                <p>A secret government agency recruits imprisoned supervillains to execute dangerous black ops missions in exchange for clemency.</p>
                <br>
                <h4 class="text-center">Score: </h4> 
                <h3 id = "score2" class="text-center">3933</h3>
                <br>
                <a class="btn btn-primary btn-lg center-block" onclick="incrementScore2()">Vote</a>
            </div>
        </div>
-->
        <!-- /.row -->

<!--        <hr>-->

<!--
        <div class="row">
            <div class="col-md-7">
                <a>
                    <img class="img-responsive img-hover" src="image/ds.jpg" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Doctor Strange</h3>
                <h5>Action | Adventure | Fantasy </h5>
                <p>After his career is destroyed, a brilliant but arrogant and conceited surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil.</p>
                <br>
                <h4 class="text-center">Score: </h4> 
                <h3 id = "score3" class="text-center">3812</h3>
                <br>
                <a class="btn btn-primary btn-lg center-block" onclick="incrementScore3()">Vote</a>
            </div>
        </div>
-->
        <!-- /.row -->

<!--        <hr>-->

<!--
        <div class="row">
            <div class="col-md-7">
                <a>
                    <img class="img-responsive img-hover" src="image/fd.jpg" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Finding Dory</h3>
                <h5>Animation | Adventure | Comedy | Family</h5>
                <p>The friendly-but-forgetful blue tang fish reunites with her loved ones, and everyone learns a few things about the real meaning of family along the way.</p>
                <br>
                <h4 class="text-center">Score: </h4> 
                <h3 id = "score4" class="text-center">3203</h3>
                <br>
                <a class="btn btn-primary btn-lg center-block" onclick="incrementScore4()">Vote</a>
            </div>
        </div>
-->
        <!-- /.row -->

<!--        <hr>-->

<!--
        <div class="row">
            <div class="col-md-7">
                <a>
                    <img class="img-responsive img-hover" src="image/id.jpg" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3>Independence Day: Resurgence</h3>
                <h5>Action | Adventure | Sci-Fi</h5>
                <p>Two decades after the first Independence Day invasion, Earth is faced with a new extra-Solar threat. But will mankind's new space defenses be enough?</p>
                <br>
                <h4 class="text-center">Score: </h4> 
                <h3 id = "score5" class="text-center">3148</h3>
                <br>
                <a class="btn btn-primary btn-lg center-block" onclick="incrementScore5()">Vote</a>
            </div>
        </div>
-->
        <!-- /.row -->

<!--        <hr>-->

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; MOVOTE 2016</p>
                </div>
            </div>
        </footer>

    </div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        var json= <?php echo(json_encode($data));?>;
        var user= <?php echo(json_encode(Auth::user()));?>;
        console.log(user);
        console.log(json);
        for (var i=0;i<json.length;i++){
            var film=json[i];
            var genres=film.genre;
            console.log(genres);
            console.log(film);
            var genreText='';
            for (var j=0;j<genres.length;j++){
                var genre=genres[j];
                if(j!==(genres.length-1)){
                    genreText+=genre.genre+' | ';
                }else{
                    genreText+=genre.genre;
                }
                
            }
            $(".content-category").append('<div class="row">'
                                         +'<div class="col-md-7">'
                                         +'<a>'
                                         +'<img class="img-responsive img-hover" src="/image/'+film.film_id+'" alt="">'
                                         +'</a>'
                                         +'</div>'
                                         +'<div class="col-md-5">'
                                         +'<h3>'+film.film_title+'</h3>'
                                         +'<h5>'+genreText+'</h5>'
                                         +'<p>'+film.film_about+'</p>'
                                         +'<h4 class="text-center">Score: </h4>'
                                         +'<h3 class="text-center" id="score_'+film.film_id+'">'+film.film_score+'</h3>'
                                         +'<br>'
                                         @if (!Auth::guest())  
                                         +'<a class="btn btn-primary btn-lg center-block vote" id="'+film.film_id+'">Vote</a>'
                                         @else
                                         +'<a class="btn btn-primary btn-lg center-block" href="{{url("login")}}">Login to Vote</a>'
                                         @endif
                                         +'</div>'
                                         +'</div>'
                                         +'<hr>'); 
             
        }
        
        $(document).on("click",".vote",function(){
            @if (!Auth::guest()) 
	       	incrementScore1("{{csrf_token()}}",user.id,"<?php echo($category);?>",this.id); 
            @endif
        });
        @if (!Auth::guest()) 
        $.ajaxSetup({
			headers:{'X-CSRF-TOKEN':"{{csrf_token()}}"}
		});
        $.get("/voting?user_id="+user.id+"&category="+"<?php echo($category);?>",function(data){
	        $("#userpoints").text(data.point);
	        
        });
        @endif
    });
</script>

@endsection