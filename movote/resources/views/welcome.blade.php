@extends('layouts.app')

@section('content')
    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('image/bvs2.jpg');"></div>
                <div class="carousel-caption">
                    <h4>"Ben Affleck and Henry Cavill clash in a bid to launch a new superhero franchise, but Zack Snyder’s shoddy adventure never gets off the ground." - Mark Kermode·The Guardian</h4>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('image/jb2.jpg');"></div>
                <div class="carousel-caption">
                    <h4>"The Jungle Book is shot beautifully, and has a fantastic sense of wonder within the world being created. That translates into big-screen magic." - Eric Eisenberg·Cinemablend</h4>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('image/cw2.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Which side are you on?</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Top Nominees</h2>
            </div>
            <div class="top-nominee">

            </div>
            <!-- <div class="col-md-4 col-sm-6">
                <a href="{{ url('/vote') }}">
                    <img class="img-responsive img-portfolio img-hover" src="image/cw3.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="{{ url('/vote') }}">
                    <img class="img-responsive img-portfolio img-hover" src="image/bvs3.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="{{ url('/vote') }}">
                    <img class="img-responsive img-portfolio img-hover" src="image/jb3.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="{{ url('/vote') }}">
                    <img class="img-responsive img-portfolio img-hover" src="image/fb.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="{{ url('/vote') }}">
                    <img class="img-responsive img-portfolio img-hover" src="image/fd.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="{{ url('/vote') }}">
                    <img class="img-responsive img-portfolio img-hover" src="image/sc3.jpg" alt="">
                </a>
            </div> -->

        </div>
        <!-- /.row -->

        <ul class="pager">
            <li class="next">
                <a href="{{ url('/vote') }}">Vote &rarr;</a>
            </li>
        </ul>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; MOVOTE 2016</p>
                </div>
            </div>
        </footer>

    </div>
@endsection

@section('js')

<script type="text/javascript"> 
    $(document).ready(function(){
        $.get('/top',function(data){
            console.log(data);
            for(var i=0; i<6; i++){
                var film = data[i];
                $('.top-nominee').append('<div class="col-md-4 col-sm-6">'
                                    + '<img class="img-responsive img-portfolio img-hover" src="image/'+ film["film_id"] +'" alt="">'
                                    +'</div>');
            }
        });
    });
</script>
@endsection
