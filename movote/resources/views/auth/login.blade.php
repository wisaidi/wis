@extends('layouts.app')

@section('content')
<div class="container" id="login-container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><h2>Login to MOVOTE</h2></div>
                <div class="panel-body">           
                <br>
                    <div class="col-md-6">
                        <a href="redirect/facebook">
                            <img class="img-responsive img-portfolio img-hover" src="image/fb_logo.png" style="width: 100px;height:100px;margin:0 auto;" alt="">
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="redirect/google">
                            <img class="img-responsive img-portfolio img-hover img-center" src="image/google_logo.png" style="width: 100px;height:100px;margin:0 auto;" alt="">
                        </a>
                    </div>
                <br>
                <hr>
                    <div class="col-md-12">
                        <h4 class="text-center">You can login using Facebook or Google account</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; MOVOTE 2016</p>
            </div>
        </div>
    </footer>
</div>

@endsection
