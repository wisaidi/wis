@extends('layouts.app')

@section('head')
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container" id="about-container">

        <!-- Jumbotron Vote -->
        <div class="jumbotron">
          <h1>MOVOTE</h1>
          <p>MOVOTE is a place for those movie-lovers to help their favourite movies to win our movie awards. Here's the thing, there are 6 MOVOTE awards, and there are 5 nominees on each award. You have 10 points on each award to increase the score of nominees. Spend your points wisely. Your simple vote means a lot to them.</p>   
          <p>Give them your support now!</p>
          <p><a class="btn btn-primary btn-lg" href="{{ url('/vote') }}" role="button">Vote</a></p>
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; MOVOTE 2016</p>
                </div>
            </div>
        </footer>

    </div>
@endsection
