@extends('layouts.app')

@section('head')
@endsection

@section('content')    
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">MOVOTE Award 2016</h2>
        </div>
        <div class="col-md-4 col-sm-6">
            <a href="{{url('category/Favourite')}}">
                <img class="img-responsive img-portfolio img-hover" src="image/fav.png" alt="">
            </a>
        </div>
        <div class="col-md-4 col-sm-6">
            <a href="{{url('category/Most%20Anticipated')}}">
                <img class="img-responsive img-portfolio img-hover" src="image/mostanticipated.png" alt="">
            </a>
        </div>
        <div class="col-md-4 col-sm-6">
            <a href="{{url('category/Shocking%20Plot%20Twist')}}">
                <img class="img-responsive img-portfolio img-hover" src="image/shocking.png" alt="">
            </a>
        </div>
        <div class="col-md-4 col-sm-6">
            <a href="{{url('category/Underrated%20Movie')}}">
                <img class="img-responsive img-portfolio img-hover" src="image/underrated.png" alt="">
            </a>
        </div>
        <div class="col-md-4 col-sm-6">
            <a href="{{url('category/Comedic%20Genius')}}">
                <img class="img-responsive img-portfolio img-hover" src="image/comedic.png" alt="">
            </a>
        </div>
        <div class="col-md-4 col-sm-6">
            <a href="{{url('category/Mind%20Bending')}}">
                <img class="img-responsive img-portfolio img-hover" src="image/mind.png" alt="">
            </a>
        </div>
    </div>
    <hr>
    <!-- /.row -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; MOVOTE 2016</p>
            </div>
        </div>
    </footer>
</div> 

@endsection